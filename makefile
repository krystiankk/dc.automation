install :
	python3 -m pip install openpyxl
	python3 -m pip install Path

generate_csv :
	python3 -W ignore ./code/generate_csv.py "$(xlsx)" "$(csv)" "$(log)"

generate_gulp :
	python3 -W ignore ./code/generate_gulp.py "$(xlsx)" "$(csv)" "$(env)" "$(log)"

generate :
	make generate_csv xlsx="$(xlsx)" csv="$(csv)" log="$(log)"
	make generate_gulp xlsx="$(xlsx)" csv="$(csv)" env="$(env)" log="$(log)"

clean_csv :
	rm -f ./csv/*.csv

clean_xlsx :
	rm -f ./xlsx/*.xlsx