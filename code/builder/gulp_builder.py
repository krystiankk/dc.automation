from _datetime import datetime
from info.logger import Logger


def check_toc(key, value):
    if key == "toc":
        if str(value).lower().startswith("c"):
            return "CH"
        else:
            return "GC"
    return value


def generate_date(mapped_obj, value):
    hour = 0
    minute = 1
    if not mapped_obj.is_from_date:
        hour = 23
        minute = 59
    return datetime \
        .strptime(str(value)[0:10], "%Y-%m-%d") \
        .replace(hour=hour, minute=minute) \
        .isoformat("T", "minutes")


def replace_specials(value):
    return str(value).translate({ord(c): "" for c in "!@#$%^&*()[]{};:,./<>?\|`~-= +"})


class GulpBuilder:
    class GulpArgument:
        def __init__(self, key, value, has_quote):
            self.key = key
            self.value = value
            self.has_quote = has_quote

    def __init__(self):
        self.attrs = []

    def append(self, key, value, mapped_obj):
        if mapped_obj.has_replace_specials:
            value = replace_specials(value)
        if mapped_obj.is_date:
            value = generate_date(mapped_obj, value)
        value = check_toc(key, value)
        self.attrs.append(self.GulpArgument(key, value, mapped_obj.has_quote))

    def add_default_gulp_argument(self, gulp_argument):
        self.attrs.append(gulp_argument)

    def build(self):
        Logger.new_line()
        gulp_command = "gulp createVouchers"
        for current in self.attrs:
            gulp_command += " --" + str(current.key)
            gulp_command += " "
            if current.has_quote:
                gulp_command += "\"" + str(current.value) + "\""
            else:
                gulp_command += str(current.value)
        return gulp_command
