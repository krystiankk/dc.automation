from info.logger import Logger
from info.no_match_found_error import NoMatchFound

maximum_operation_count = 50000
cols = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
        "W", "X", "Y", "Z"]


class XlsxCrawlerHelper:
    @staticmethod
    def find_string(sheet, string, start_row, start_col):
        Logger.info(
            "XlsxCrawler is finding '" + Logger.decorate_green(string) + "' from position" + XlsxCrawlerHelper.p2(
                start_row, start_col))
        op_count = 0
        row = start_row
        col = start_col
        while op_count < maximum_operation_count and sheet[cols[col] + str(row)].value != string:
            if col < len(cols) - 1:
                col = col + 1
            else:
                col = start_col
                row = row + 1
            op_count = op_count + 1
        if op_count >= maximum_operation_count:
            raise NoMatchFound("XlsxCrawler has not fount '" + Logger.decorate_green(string) + "'")
        Logger.ok(
            "XlsxCrawler has found '" + Logger.decorate_green(string) + "' in position" + XlsxCrawlerHelper.p2(row,
                                                                                                               col))
        return [row, col]

    @staticmethod
    def search_for_value(sheet, start_row, start_col, offset, col_upd=0, row_upd=1):
        Logger.info("XlsxCrawler is searching for value from position:" +
                    XlsxCrawlerHelper.p2(start_row, start_col) +
                    " with maximum values to check: " + Logger.decorate_blue(str(offset)))
        while sheet[cols[start_col + col_upd] + str(start_row + row_upd)].value is None and offset > 0:
            start_row = start_row + row_upd
            start_col = start_col + col_upd
            offset = offset - 1
        if sheet[cols[start_col + col_upd] + str(start_row + row_upd)].value is None:
            Logger.warning("XlsxCrawler has NOT found any value, raising exception")
            raise NoMatchFound(
                "I could not find matching value, last checked position" + XlsxCrawlerHelper.p2(start_row, start_col))
        else:
            Logger.ok("XlsxCrawler has found matching value: " + Logger.decorate_blue(str(
                sheet[cols[start_col + col_upd] + str(start_row + row_upd)].value)))
            return sheet[cols[start_col + col_upd] + str(start_row + row_upd)].value

    @staticmethod
    def find_attr_value(sheet, name, is_prod, position):
        try:
            if is_prod:
                Logger.info("Trying find prod values in the right side of the document...")
            col = position[1] if not is_prod else 8
            position = XlsxCrawlerHelper.find_string(sheet, name, position[0], col)
            if is_prod:
                Logger.success("Found prod value!")
        except NoMatchFound:
            if is_prod:
                Logger.warning("Right side value has not been found, trying on the left side...")
            position = XlsxCrawlerHelper.find_string(sheet, name, position[0], position[1])

        return XlsxCrawlerHelper.search_for_value(sheet, position[0], position[1], 5, 1, 0)

    @staticmethod
    def p1(position):
        return Logger.decorate_purple(" " + cols[position[1]] + str(position[0]))

    @staticmethod
    def p2(row, col):
        return XlsxCrawlerHelper.p1([row, col])
