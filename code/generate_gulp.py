import openpyxl
import sys
from pathlib import Path
from processor.gulp_processor import GulpProcessor
from info.logger import Logger

XLSX_file_name = "./xlsx/" + str(sys.argv[1])  # name of XLSX file to generate CSV file from.
CSV_file_name = "./csv/" + str(sys.argv[2])  # name of CSV file to generate.
is_prod = sys.argv[3] is not None and sys.argv[3] == "PROD"  # name of CSV file to generate.
is_logging = sys.argv[4] is not None and sys.argv[4] == "full"
Logger.IS_LOGGING = True
Logger.info("Building gulp command...")
Logger.IS_LOGGING = is_logging  # This is for better visibility in console, set to True for debug purpose

Logger.header("Arguments: ")
Logger.argument("XLSX file: " + XLSX_file_name)
Logger.argument("CSV file: " + CSV_file_name)
Logger.argument("is_prod " + str(is_prod))
xlsx_file = Path(XLSX_file_name)
wb_obj = openpyxl.load_workbook(xlsx_file, data_only=True)
document_starting_pos = [1, 0]  # we are starting at row = 1 and col = 0 = A
processor = GulpProcessor(wb_obj["Promotion Builder"], document_starting_pos, str(sys.argv[2]), is_prod)
processor.run()
Logger.summarise(str(sys.argv[3]))
Logger.diff_line()
