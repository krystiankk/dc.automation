import openpyxl
import sys
from pathlib import Path
from processor.csv_processor import CSVProcessor
from info.logger import Logger
from crawler.xlsx_crawler_helper import XlsxCrawlerHelper

Logger.IS_LOGGING = True
Logger.info("Building csv file...")
XLSX_file_name = "./xlsx/" + str(sys.argv[1])  # name of XLSX file to generate CSV file from.
CSV_file_name = "./csv/" + str(sys.argv[2])  # name of CSV file to generate.
is_logging = sys.argv[3] is not None and sys.argv[3] == "full"
Logger.IS_LOGGING = is_logging  # This is for better visibility in console, set to True for debug purpose
Logger.header("Arguments: ")
Logger.argument("XLSX file: " + XLSX_file_name)
Logger.argument("CSV file: " + CSV_file_name)
xlsx_file = Path(XLSX_file_name)
wb_obj = openpyxl.load_workbook(xlsx_file, data_only=True)
document_starting_pos = [1, 0]  # we are starting at row = 1 and col = 0 = A

csv_processor = CSVProcessor(wb_obj["Route Configuration - NLC" if "NLC" in str(
    XlsxCrawlerHelper.find_attr_value(wb_obj["Promotion Builder"], "Route sheet to use:", False,
                                      document_starting_pos)) else "Route Configuration - CRS"], document_starting_pos,
                             CSV_file_name)
csv_processor.run()
Logger.new_line()
Logger.summarise(sys.argv[1])
