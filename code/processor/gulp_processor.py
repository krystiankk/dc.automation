from builder.gulp_builder import GulpBuilder
from crawler.xlsx_crawler_helper import XlsxCrawlerHelper
from info.logger import Logger

no_name = "NO_NAME"
default = "DEFAULT"


class GulpProcessor:
    class MappedObj:
        def __init__(self, name, has_quote, has_replace, is_date, is_from_date=True):
            self.name = name
            self.has_quote = has_quote
            self.has_replace_specials = has_replace
            self.is_date = is_date
            self.is_from_date = is_from_date

    def __init__(self, sheet, position, csv_file_name, is_prod):
        self.sheet = sheet
        self.position = position
        self.csv_file_name = csv_file_name
        self.is_prod = is_prod
        self.gulpBuilder = GulpBuilder()

        # Default values that are not specified in proforma and same for every discount code.
        # Should be modified if needed.
        self.default_values = {"promotionType": GulpBuilder.GulpArgument("promotionType", "VOUCHER", False),
                               "details": GulpBuilder.GulpArgument("details", "TBC", True),
                               "isPercent": GulpBuilder.GulpArgument("isPercent", "true", False),
                               "pathPomotionRuleSet": GulpBuilder.GulpArgument("pathPomotionRuleSet", csv_file_name,
                                                                               True)}
        self.gulpMappings = {
            "toc": self.MappedObj("TOC:", False, False, False),
            "validityFrom": self.MappedObj("Earliest booking date:", False, False, True),
            "validityTill": self.MappedObj("Latest Booking Date:", False, False, True, False),
            "name": self.MappedObj("Promotion Name:", True, False, False),
            "promotionType": default,
            "prefixCode": self.MappedObj("Promotion Code Prefix:", True, True, False),
            "useCount": self.MappedObj("Usage count:", False, False, False),
            "discount": self.MappedObj("Discount %:", False, False, False),
            "isPercent": default,
            "count": self.MappedObj("UAT Batch size:", False, False, False) if not is_prod else self.MappedObj(
                "PROD Batch size:", False, False, False),
            "departureDateFrom": self.MappedObj("Earliest departure date:", False, False, True),
            "departureDateTo": self.MappedObj("Latest departure Date:", False, False, True, False),
            "returnDateFrom": self.MappedObj("Earliest return date:", False, False, True),
            "returnDateTo": self.MappedObj("Latest return Date:", False, False, True, False),
            "passengersMax": self.MappedObj("Total passengers (max):", False, False, False),
            "passengersMin": self.MappedObj("Total passengers (min):", False, False, False),
            "adultsMax": self.MappedObj("No. Adults (Max):", False, False, False),
            "adultsMin": self.MappedObj("No. Adults (Min):", False, False, False),
            "childrenMax": self.MappedObj("No. Children (Max):", False, False, False),
            "childrenMin": self.MappedObj("No. Children (Min):", False, False, False),
            "description": self.MappedObj("Promotion Description:", True, False, False),
            "details": default,
            "pathPomotionRuleSet": default
        }

    def append(self, gulp_name, mapped_obj):
        if mapped_obj == default:
            self.gulpBuilder.add_default_gulp_argument(self.default_values[gulp_name])
        else:
            self.gulpBuilder.append(gulp_name,
                                    XlsxCrawlerHelper.find_attr_value(self.sheet, mapped_obj.name, self.is_prod,
                                                                      self.position),
                                    mapped_obj)

    def run(self):
        for key in self.gulpMappings:
            self.append(key, self.gulpMappings[key])
        parts = str(self.csv_file_name).split(".")
        gulp_command = self.gulpBuilder.build()
        file_name = "./txt/" + parts[0] + "_GULP_" + ("PROD.txt" if self.is_prod else "UAT_TEST.txt")
        with open(file_name, 'a') as file:
            file.truncate(0)
            file.write(gulp_command)

        Logger.quiet_data(" 1. Generated txt file with gulp command: ", file_name)
        Logger.quiet_data(" 2. Gulp command: ", gulp_command)
