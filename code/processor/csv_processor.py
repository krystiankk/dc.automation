from info.no_match_found_error import NoMatchFound
from info.logger import Logger
from crawler.xlsx_crawler_helper import XlsxCrawlerHelper, cols

maximum_operation_count = 1000


class CSVProcessor:
    def __init__(self, route_sheet, position, csv_file_name):
        self.route_sheet = route_sheet
        self.position = position
        self.overall_counter = 0
        self.csv_file_name = csv_file_name
        Logger.new_line()
        Logger.success("CSVProcessor initialized...")

    def __find_nlc_codes(self):
        Logger.info(
            "CSVProcessor is finding NLC codes from position" + XlsxCrawlerHelper.p1(self.position))
        nlc_pos = XlsxCrawlerHelper.find_string(self.route_sheet, "NLC", self.position[0], self.position[1])

        Logger.info("CSVProcessor is shrinking search area for next search, old coordinates:" + XlsxCrawlerHelper.p1(
            self.position))
        self.position[0] = nlc_pos[0] - 1
        self.position[1] = nlc_pos[1] + 1
        Logger.info("CSVProcessor is shrinking search area for next search, new coordinates:" + XlsxCrawlerHelper.p1(
            self.position))

        nlc_pos[0] = nlc_pos[0] + 1
        Logger.info("CSVProcessor probable NLC starting position:" + XlsxCrawlerHelper.p1(nlc_pos))
        nlc_codes = []
        while self.route_sheet[cols[nlc_pos[1]] + str(nlc_pos[0])].value is not None:
            Logger.ok("CSVProcessor has found NLC code: " + Logger.decorate_blue(str(
                self.route_sheet[cols[nlc_pos[1]] + str(nlc_pos[0])].value)))
            nlc_codes.append(self.route_sheet[cols[nlc_pos[1]] + str(nlc_pos[0])].value)
            nlc_pos[0] = nlc_pos[0] + 1

        return nlc_codes

    def __find_return_routes(self):
        Logger.info("CSVProcessor is finding return routes")
        try:
            Logger.info("CSVProcessor trying label 'Include Return Route?'")
            return_pos = XlsxCrawlerHelper.find_string(self.route_sheet, "Include Return Route?", self.position[0],
                                                       self.position[1])
        except NoMatchFound:
            Logger.warning("CSVProcessor Label 'Include Return Route?' not found, trying: 'Include Reverse Route?'")
            return_pos = XlsxCrawlerHelper.find_string(self.route_sheet, "Include Reverse Route?", self.position[0],
                                                       self.position[1])
        return_pos[0] = return_pos[0] + 2
        Logger.info("CSVProcessor probable return routes starting position:" + XlsxCrawlerHelper.p1(return_pos))
        return_included = []
        while self.route_sheet[cols[return_pos[1]] + str(return_pos[0])].value is not None:
            Logger.ok("CSVProcessor return route found:" + Logger.decorate_blue(str(
                self.route_sheet[cols[return_pos[1]] + str(return_pos[0])].value)))
            if self.route_sheet[cols[return_pos[1]] + str(return_pos[0])].value == "Yes":
                return_included.append(True)
            else:
                return_included.append(False)
            return_pos[0] = return_pos[0] + 1
        return return_included

    def __find_fare_codes(self):
        Logger.info("CSVProcessor is finding fare codes")
        fare_codes = XlsxCrawlerHelper.find_string(self.route_sheet,
                                                   "Fare Codes / Ticket Types to be Discounted on these Routes",
                                                   self.position[0],
                                                   self.position[1])
        start_pos = [fare_codes[0] + 2, fare_codes[1]]

        Logger.info("CSVProcessor is shrinking search area for next search, old coordinates:" + XlsxCrawlerHelper.p1(
            self.position))
        self.position[0] = fare_codes[0]
        Logger.info("CSVProcessor is shrinking search area for next search, new coordinates:" + XlsxCrawlerHelper.p1(
            self.position))

        none_counter = 0
        arr = []
        while none_counter < 2:
            if self.route_sheet[cols[start_pos[1]] + str(start_pos[0])].value is not None:
                Logger.ok("CSVProcessor has found fare code: " + Logger.decorate_blue(str(
                    self.route_sheet[cols[start_pos[1]] + str(start_pos[0])].value)))
                arr.append(self.route_sheet[cols[start_pos[1]] + str(start_pos[0])].value)
                start_pos[0] = start_pos[0] + 1
                none_counter = 0
            else:
                none_counter = none_counter + 1
                start_pos[1] = start_pos[1] + 1
                start_pos[0] = fare_codes[0] + 2

        return arr

    def __generate_route_code_part(self):
        route_code = XlsxCrawlerHelper.find_string(self.route_sheet, "Route Code", self.position[0], self.position[1])
        route_code = int(
            XlsxCrawlerHelper.search_for_value(self.route_sheet, route_code[0], route_code[1], 3))
        Logger.ok("Route code found: " + Logger.decorate_blue(str(route_code)))
        origin_nlc = self.__find_nlc_codes()
        destination_nlc = self.__find_nlc_codes()
        self.position[1] = 0
        return_route = self.__find_return_routes()
        fare_codes = self.__find_fare_codes()
        import _csv
        Logger.info(
            "CSVProcessor appending csv file: + " + Logger.decorate_purple(self.csv_file_name) +
            " for route code: " + Logger.decorate_blue(str(route_code)) + "...")
        with open(self.csv_file_name, 'a') as file:
            count = 0
            writer = _csv.writer(file)
            for fare in fare_codes:
                for i in range(len(origin_nlc)):
                    writer.writerow([origin_nlc[i], destination_nlc[i], route_code, fare])
                    count = count + 1
                    if return_route[i]:
                        writer.writerow([destination_nlc[i], origin_nlc[i], route_code, fare])
                        count = count + 1
        self.overall_counter = self.overall_counter + count
        Logger.ok("CSVProcessor inserted " +
                  Logger.decorate_bold(Logger.decorate_green(str(count))) +
                  " entries for route code: " + Logger.decorate_blue(str(route_code)))

    def run(self):
        has_route_code = True
        route_codes_counter = 0
        with open(self.csv_file_name, 'a') as file:
            file.truncate(0)
        while has_route_code:
            try:
                Logger.new_line()
                Logger.header("CSVProcessor looking for next route code...")
                self.__generate_route_code_part()
                route_codes_counter = route_codes_counter + 1
            except NoMatchFound:
                Logger.info("CSVProcessor no more valid route codes found :(")
                Logger.info("finishing process..")
                Logger.quiet_data(" 1. found route codes: ", str(route_codes_counter))
                Logger.quiet_data(" 2. inserted csv entries: ", str(self.overall_counter))
                Logger.quiet_data(" 3. generated CSV file: ", str(self.csv_file_name))
                has_route_code = False
