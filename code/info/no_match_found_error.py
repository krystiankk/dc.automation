from info.logger import Logger


class NoMatchFound(BaseException):
    def __init__(self, message):
        Logger.error("[NoMatchFound] " + message)
        pass
