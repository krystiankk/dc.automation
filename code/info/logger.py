HEADER = '\033[95m'
OK_BLUE = '\033[94m'
OK_GREEN = '\033[92m'
WARNING = '\033[93m'
ARGUMENT = '\033[90m'
FAIL = '\033[91m'
END_C = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'


class Logger:
    IS_LOGGING = True

    @staticmethod
    def header(text):
        if Logger.IS_LOGGING:
            print(HEADER + BOLD + text + END_C)

    @staticmethod
    def argument(text):
        if Logger.IS_LOGGING:
            print(ARGUMENT + BOLD + text + END_C)

    @staticmethod
    def info(text):
        if Logger.IS_LOGGING:
            print(OK_BLUE + "[INFO] " + END_C + text)

    @staticmethod
    def ok(text):
        if Logger.IS_LOGGING:
            print(OK_GREEN + " [OK] " + END_C + text)

    @staticmethod
    def success(text):
        if Logger.IS_LOGGING:
            print(OK_GREEN + BOLD + UNDERLINE + text + END_C)

    @staticmethod
    def quiet_success(text):
        print(OK_GREEN + text + END_C)

    @staticmethod
    def quiet_data(title, text):
        print(ARGUMENT + BOLD + title + END_C + OK_GREEN + text + END_C + "\n")

    @staticmethod
    def summarise(text):
        print(ARGUMENT + BOLD + "GENERATED FOR: " + END_C + HEADER + BOLD + UNDERLINE + text + END_C + "\n")

    @staticmethod
    def diff_line():
        print(
            ARGUMENT + BOLD + "================================== FINISHED ==================================" + END_C + "\n")

    @staticmethod
    def warning(text):
        if Logger.IS_LOGGING:
            print(WARNING + " [WARNING] " + END_C + text)

    @staticmethod
    def error(text):
        if Logger.IS_LOGGING:
            print(FAIL + "[ERROR] " + END_C + text)

    @staticmethod
    def new_line():
        if Logger.IS_LOGGING:
            print("\n")

    @staticmethod
    def decorate_purple(text):
        return HEADER + text + END_C

    @staticmethod
    def decorate_green(text):
        return OK_GREEN + text + END_C

    @staticmethod
    def decorate_blue(text):
        return OK_BLUE + BOLD + text + END_C

    @staticmethod
    def decorate_bold(text):
        return BOLD + text + END_C
