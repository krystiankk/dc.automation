# Discount code generation automation script
## Version 1.0

## <~ Python version: 3.7 ~>

This is a script for generating CSV files and gulp commands from discount code proformas.

## How it works?

* ### CSV generation

    Every csv file is a combination of origin NLC/CRS code, destination NLC/CRS code, route code and fare code in that order.
    To generate csv file, `csv_processor` is finding required values by looking below labels (due to proforma format). 
    If it cannot find any value in few fields below desired label, then if required value is for route code label - it means that we have already generated all csv data from that proforma; else means that we have iterated over all interesting values in this column for current label 
    Supporting many route codes is allowed by shrinking search area during searching - so next route code search won't be the same as previous one.
    Information about which sheet to use for generation (NLC / CSV) is provided in main proforma sheet and used automatically by this script. 
    
* ### GULP generation

    Gulp command is generated from main proforma sheet.
    Almost all required information is provided there and found by label (on the right side of label this time).
    Some key note to take here:
    
    1.  Dates are in "yyyy-MM-ddTHH:mm" format, like: `2020-08-01T00:01`
    2.  We have to differentiate between PROD and UAT gulp commands and use appropriate fields.
    3.  Separation between PROD and UAT configuration is achieved by trying to look in right side of the document first for PROD values (due to proforma format), 
    if value is not found on the right side, we will look on the left side.

* ### Iterating over xlsx document

    Python `openpyxl` is used for iterating over xlsx document. It supports accessing any file by simply:
    
    ```
    xlsx_file = Path(XLSX_file_name)
  wb_obj = openpyxl.load_workbook(xlsx_file, data_only=True)
    ```
  
    `data_only` attribute is used for evaluating any functions that are in xlsx file. 
    Then you can open any sheet by using:
    
    ```
    wb_obj["Promotion Builder"]
    ```
  
    Where `Promotion Builder` is sheet name. 
    When you have opened your sheet, access every cell in row+col manner, like: `5C` - where `5` corresponds to 5th row and `C` corresponds to 3rd column.


## Steps to use it:

1.  Clone this repository
2.  Run `make install` command to install required packages
3.  Copy your `<xlsx_filename.xlsx>` file to *xlsx* folder
4.  Run `make generate xlsx="<xlsx_filename.xlsx>" csv="<csv_filename.csv>" env="<PROD / UAT>"`
5.  Generated `<csv_filename.csv>` file should be inside *csv* folder
6.  You should see generated gulp command in your console
7.  Generated `<csv_filename>_GULP_<PROD/UAT>.txt>` file containing gulp command  should be inside *txt* folder


Ad. 4 - "PROD" or "UAT" Refers too which proforma fields should be used for gulp command generation.

Example generation command:
`make generate xlsx="GC50 50 percent tactical 200730.xlsx" csv="GC50 50 percent tactical 200730.csv" env="PROD"`

#### Inside both `csv_processor.py` and `gulp_processor.py` there is a flag (`Logger.IS_LOGGING`) for enabling extended console logging - might be helpful for debugging purpose.

### Other commands:

1.  `make clean_csv` - remove all csv files from main project directory
2.  `make clean_xlsx` - remove all xlsx files from main project directory
3.  `make generate_gulp xlsx="<xlsx_filename.xlsx>" csv="<csv_filename.csv>" env="<PROD / UAT>"` - generate only gulp command
4.  `make generate_csv xlsx="<xlsx_filename.xlsx>" csv="<csv_filename.csv>"` - generate only csv file

#### Example generation, console content, debug logging OFF

![Example usage result](https://github.com/software-bot/particles/blob/master/img.png?raw=true)

#### Example generation, small part of console content, debug logging ON

![Example usage result](https://github.com/software-bot/particles/blob/master/logging.png?raw=true)
